package sk.mvozny.transakcie.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import sk.mvozny.transakcie.service.TestServiceOuter;

@RestController
public class TestController {

    @Autowired
    TestServiceOuter testServiceOuter;

    @GetMapping("/test")
    public void test(){
        testServiceOuter.saveTest();
    }
}
