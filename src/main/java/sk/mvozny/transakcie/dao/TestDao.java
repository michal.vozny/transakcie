package sk.mvozny.transakcie.dao;

import org.springframework.stereotype.Repository;

import sk.mvozny.transakcie.entity.TestEntity;

@Repository
public class TestDao extends AbstractCommonDao {
    public void saveTestEntity(TestEntity entity){
        entityManager.persist(entity);
        entityManager.flush();
    }
}
