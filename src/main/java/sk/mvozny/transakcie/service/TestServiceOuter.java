package sk.mvozny.transakcie.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TestServiceOuter {

    @Autowired
    TestServiceInner testServiceInner;

    public void saveTest(){
        testServiceInner.saveTest1();
//        testServiceInner.saveTestRuntimeError();

        try {
            testServiceInner.saveTestDbError();
        }
        catch (Exception e){
            System.out.println("odchytena chyba " + e.getMessage());
        }
    }
}
