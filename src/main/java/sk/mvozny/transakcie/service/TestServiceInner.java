package sk.mvozny.transakcie.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sk.mvozny.transakcie.dao.TestDao;
import sk.mvozny.transakcie.entity.TestEntity;

@Service
@Transactional
public class TestServiceInner {

    @Autowired
    TestDao testDao;

    public void saveTest1(){
        TestEntity entity = new TestEntity();
        entity.setText("zaznam 1");
        entity.setNumUq(1L);
        testDao.saveTestEntity(entity);
    }

    public void saveTestRuntimeError(){
        TestEntity entity = new TestEntity();
        entity.setText("zaznam 2");
        entity.setNumUq(2L);
        testDao.saveTestEntity(entity);

        throw new RuntimeException("nejaka chyba");
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void saveTestRuntimeErrorInNewTransaction(){
        TestEntity entity = new TestEntity();
        entity.setText("zaznam 2");
        entity.setNumUq(2L);
        testDao.saveTestEntity(entity);

        throw new RuntimeException("nejaka chyba");
    }

    public void saveTestDbError(){
        TestEntity entity = new TestEntity();
        entity.setText("zaznam 1");
        entity.setNumUq(1337L); //takyto zaznam uz je v db, takze nastane uniqueconstraintexception
        testDao.saveTestEntity(entity);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void saveTestDbErrorInNewTransaction(){
        TestEntity entity = new TestEntity();
        entity.setText("zaznam 1");
        entity.setNumUq(1337L); //takyto zaznam uz je v db, takze nastane uniqueconstraintexception
        testDao.saveTestEntity(entity);
    }
}
