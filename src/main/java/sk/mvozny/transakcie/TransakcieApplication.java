package sk.mvozny.transakcie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransakcieApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransakcieApplication.class, args);
	}

}
